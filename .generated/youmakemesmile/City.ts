enum City {
	 Seoul = 'Seoul', // code:"S", value:"서울"
	 Busan = 'Busan', // code:"B", value:"부산"
}

const CityLabel = new Map<City, string>([
	[City.Seoul, '서울'],
	[City.Busan, '부산'],
]);

export {
	City,
	CityLabel,
};
