package io.velog.youmakemesmile;

public enum City {
    Seoul("S", "서울"),
    Busan("B", "부산");


    City(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private String code;
    private String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
